
# UPDATE THE SOURCE
sudo apt update


# Initial server set up steps and settign up firewall to block certain ports
# https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04
# To be done at a later stage



# Install GIT
sudo apt -y install git

# install apache2
sudo apt -y install apache2

# Install Mysql server
sudo apt -y install mysql-server

# http://bertvv.github.io/notes-to-self/2015/11/16/automating-mysql_secure_installation/
# https://github.com/bertvv/scripts/blob/8e6adbb1eb162ddf93bb1792ec7c47d9b5355f19/src/secure-mysql.sh
# ./mysql_secure_installation_automated.sh


#https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
## !!!!!!!!!! IMportant to execute the 3rd step in tutorial - ie, set mysql_native_password as the method to login.
sudo mysql_secure_installation




# Create Non root user
#details here https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04


# Telegram Desktop
sudo apt -y install telegram-desktop

# Filezilla
sudo apt -y install filezilla

# Download and Install 
# - Visual Studio Code | install using dpkg -i DEB_FILE_NAME

# Install php
sudo apt -y install php
sudo apt -y install php-mysql

# ------------------------------------------------------------
# additional modules, which will enhance functionality of PHP.
# ------------------------------------------------------------
#GD library
sudo apt -y install php-gd

#zip
sudo apt -y install php-zip

#CURL
sudo apt -y install php-curl

#------------------------------------------------------------------------------


# restart apache server
sudo systemctl restart apache2

#------------------------
# configuring www folder
#------------------------

#add current user to www-data group
sudo adduser $USER www-data

# change ownership of www folder
sudo chown $USER:www-data -R /var/www/html

# not sure of this, so keeping it on hold. (set appropriate permissions for the html/ folder.)
# sudo chmod u=rwX,g=srwX,o=rX -R /var/www/html

# set appropriate permissions for the html/ folder.
# set the GUID  of html/ folder.("s" option)
# so that folders created under it will be created with group permissions.
sudo chmod u=rwx,g=rwxs,o=rx -R /var/www/html



# Configure php.ini settings
post_max_size
upload_max_filesize

# mysql workbench
sudo apt -y install mysql-workbench



#enable rewrite module in apache
sudo a2enmod rewrite

#edit apache config file to allow all.
AllowOverride All

# restart apache 
systemctl restart apache2




#------------------------
