# Setup server on debian

## https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10

## remove DC ROM option from sources file.

## add user to sudo group

adduser sammy

usermod -aG sudo rakesh


## UFW profile for open ssh is added only after adding this software
apt install -y openssh-server


## Install Firewall
apt install ufw

## We need to make sure that the firewall allows SSH connections so that we can log back in next ## time. We can allow these connections by typing:

ufw allow OpenSSH

## Afterwards, we can enable the firewall by typing:

ufw enable

## ufw status

## Install Maria DB 


https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-10



## Telegram Desktop
sudo apt -y install telegram-desktop

## Filezilla
sudo apt -y install filezilla

## Download and Install 
## - Visual Studio Code | install using dpkg -i DEB_FILE_NAME

## Install php
sudo apt -y install php
sudo apt -y install php-mysql

## ------------------------------------------------------------
## additional modules, which will enhance functionality of PHP.
## ------------------------------------------------------------

##GD library
sudo apt -y install php-gd

##zip
sudo apt -y install php-zip

##CURL
sudo apt -y install php-curl


# restart apache server
sudo systemctl restart apache2

#------------------------
# configuring www folder
#------------------------

#add current user to www-data group
sudo adduser $USER www-data

# change ownership of www folder
sudo chown $USER:www-data -R /var/www/html

# not sure of this, so keeping it on hold. (set appropriate permissions for the html/ folder.)
# sudo chmod u=rwX,g=srwX,o=rX -R /var/www/html

# set appropriate permissions for the html/ folder.
# set the GUID  of html/ folder.("s" option)
# so that folders created under it will be created with group permissions.
sudo chmod u=rwx,g=rwxs,o=rx -R /var/www/html



# Configure php.ini settings
post_max_size
upload_max_filesize

## mysql workbench

https://dev.mysql.com/downloads/repo/apt/

dpkg -i deb_file_name

### requires that we install wget

Could not install mysql work bench - abandoned at that.
